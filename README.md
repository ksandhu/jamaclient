# jamaclient
This module is a REST Client for Jama in Python.

It provides programmatic access to various Jama features.

## Usage
Create a JSON configuration file with the following information schema:

	{
		'username': 'required',
		'base url': 'optional',
		'project id': 0,
		'item type id': 0
	}
where 'project id' and 'item type id' are optional.

Running sample main program:

	python3 jamaclient.py -c "<path to JSON config file>" -p "<Jama Password>"

In other packages, use the following:

	from jamaclient import JamaClient

## Installation
Ensure setuptools is installed.  Then, download the source package from the project page on [BitBucket](https://bitbucket.org/ksandhu/jamaclient/overview) and run the following:

    python3 setup.py install

Ensure that the location of the install is on your system's PATH.

## For Developers
To Generate Doxygen Documentation, first install doxygen.  Then, in the jamautils directory, run the following command:
 
    doxygen doxygenconfig

Documentation will be generated in the docs directory.

## Exceptions
When creating an instance of the Jama Client, the following exception can be thrown:

	@throws: jamaexceptions.InputError if <config> is missing required fields

All main functions can throw the following exceptions:
	
	@throws: jamaexceptions.BadRequestResponseError if request could not be parsed or
               parameters were not valid
    @throws: jamaexceptions.UnauthorizedResponseError if username/password is invalid
               or user does not have access to requested object
    @throws: jamaexceptions.NotFoundResponseError if the object requested does not exist
               at the specified location
    @throws: jamaexceptions.MethodNotAllowedResponseError if there is an issue with the
               way the request was made
    @throws: jamaexceptions.TooManyRequestsResponseError if there is API throttling or
               system maintenance
    @throws: jamaexceptions.UnhandledResponseError for all other error responses to requests

## Schemas

### projects

	{
		"id": 0,
		"projectKey": "string",
		"parent": 0,
		"isFolder": true,
		"createdDate": "2018-02-28T23:44:53.241Z",
		"modifiedDate": "2018-02-28T23:44:53.241Z",
		"createdBy": 0,
		"modifiedBy": 0,
		"fields": []
	}

### abstractitems, items

	{
		"id": 0,
		"documentKey": "string",
		"globalId": "string",
		"project": 0,
		"itemType": 0,
		"childItemType": 0,
		"createdDate": "2018-02-28T23:44:53.241Z",
		"modifiedDate": "2018-02-28T23:44:53.241Z",
		"lastActivityData": "2018-02-28T23:44:53.241Z",
		"createdBy": 0,
		"modifiedBy": 0,
		"lock": {
			"locked": true,
			"lastLockedDate": "2018-02-28T23:44:53.241Z",
			"lockedBy": 0
		},
		"location": {
			"sortOrder": 0,
			"globalSortOrder": 0,
			"sequence": "string",
			"parent": {
				"project": 0,
				"item": 0
			}
		},
		"resources": {},
		"fields": {}
	}

### itemtypes

    {
    	"id": 0,
    	"typeKey": "string",
    	"display": "string"
    	"displayPlural": "string",
    	"description": "string",
    	"image": "string",
    	"category": "CORE",
    	"widgets": [
    		{
    			"name": "ACTIVITIES",
    			"synchronize": true
    		}
    	],
    	"fields": [
    		{
    			"id": 0,
    			"name": "string",
    			"label": "string",
    			"fieldType": "INTEGER",
    			"readOnly": true,
    			"readOnlyAllowedApiOverwrite": true,
    			"required": true,
    			"triggerSuspect": true,
    			"synchronize": true,
    			"pickList": 0,
    			"textType": "TEXTAREA",
    			"itemType": 0
    		}
    	],
    	"system": true
    }

### tags

	{
		"id": 0,
		"name": "string",
		"project": 0
	}

### testplans

	{
		"id": 0,
		"documentKey": "string",
		"globalId": "string",
		"project": 0,
		"itemType": 0,
		"createdDate": "2018-02-28T23:44:53.241Z",
		"modifiedDate": "2018-02-28T23:44:53.241Z",
		"lastActivityData": "2018-02-28T23:44:53.241Z",
		"createdBy": 0,
		"modifiedBy": 0,
		"resources": {},
		"fields": {},
		"archived": true
	}

### testgroups

	{
		"id": 0,
		"name": "string",
		"assignedTo": 0
	}

### testcycles

	{
		"id": 0,
		"documentKey": "string",
		"globalId": "string",
		"project": 0,
		"itemType": 0,
		"createdDate": "2018-02-28T23:44:53.241Z",
		"modifiedDate": "2018-02-28T23:44:53.241Z",
		"lastActivityData": "2018-02-28T23:44:53.241Z",
		"createdBy": 0,
		"modifiedBy": 0,
		"resources": {},
		"fields": {}
	}

### testruns

	{
		"id": 0,
		"documentKey": "string",
		"globalId": "string",
		"project": 0,
		"itemType": 0,
		"createdDate": "2018-02-28T23:44:53.241Z",
		"modifiedDate": "2018-02-28T23:44:53.241Z",
		"lastActivityData": "2018-02-28T23:44:53.241Z",
		"createdBy": 0,
		"modifiedBy": 0,
		"testCaseVersionNumber": 0,
		"sortOrderFromTestGroup": 0,
		"testGroup": [
			0,
		],
		"resources": {},
		"fields": {},
	}
