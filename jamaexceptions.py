"""@package docstring

Created on Feb 14, 2018

@author: katelyn.sandhu

@description: Exceptions for the JamaClient
"""


class InputError(KeyError):
    """ Raise when the configuration file input is missing a required input value.

    Subclass of KeyError built-in exception.
    """

    def __init__(self, message):
        """ Constructor of InputError exception.

        @param message: <string> The message to pring for the exception.
        """
        self.message = message
        super(InputError, self).__init__(self.message)


class BadRequestResponseError(Exception):
    """ Raise when a request to the Jama REST API could not be parsed or the parameters
          were not valid.  The request should be modified before resubmitting.

    Subclass of Exception built-in exception.
    """

    def __init__(self, request_url):
        """ Constructor of BadRequestResponseError.

        @param request_url: <string>  The URL of the request that was made.
        """
        self.message = "The request using the URL [" + \
                       request_url + \
                       "] could not be parsed or the parameters were not valid. " + \
                       "The request should be modified before resubmitting."
        super(BadRequestResponseError, self).__init__(self.message)


class UnauthorizedResponseError(Exception):
    """ Raise when the Username/Password is invalid or the user does not have
          access to the requested object using the Jama REST API.

    Subclass of Exception built-in exception.
    """

    def __init__(self, request_url):
        """ Constructor of UnauthorizedResponseError.

        @param request_url: <string> The URL of the request that was made.
        """
        self.message = "The Username/Password is invalid or the user does not have " + \
                       "access to the requested object made using the URL [" + request_url + "]."
        super(UnauthorizedResponseError, self).__init__(self.message)


class NotFoundResponseError(Exception):
    """ Raise when the syntax is correct on the Jama REST API request, but the
          requested object does not exist at the location specified.

    Subclass of Exception built-in exception.
    """

    def __init__(self, request_url):
        """ Constructor of NotFoundResponseError.

        @param request_url: <string> The URL of the request that was made
        """
        self.message = "The request syntax is correct; however, the requested object " + \
                       "does not exist at the request URL [" + request_url + "]."
        super(NotFoundResponseError, self).__init__(self.message)


class MethodNotAllowedResponseError(Exception):
    """ Raise when there was an issue with the way the Jama REST API request was made.

    Subclass of Exception built-in exception.
    """

    def __init__(self, request_url):
        """ Constructor of MethodNotAllowed.

        @param request_url: <string> The URL of the request that was made
        """
        self.message = "There was an issue with the way the request using " + \
                       "the URL [" + request_url + "] was made."

        super(MethodNotFoundResponseError, self).__init__(self.message)


class TooManyRequestsResponseError(Exception):
    """ Raise when the Jama REST API is being throttled or the Jama is down for
          system maintenance.

    Subclass of Exception built-in exception.
    """

    def __init__(self, request_url):
        """ Constructor of TooManyRequestsResponseError.

        @param request_url: <string> The URL of the request that was made
        """
        self.message = "Jama REST API is either being throttled due to too many requests " + \
                       "or Jama is down for maintenance.  Request URL: [" + request_url + "]."
        super(TooManyRequestsResponseError, self).__init__(self.message)


class UnhandledResponseError(Exception):
    """ Raise when an unhandled response status code is received from a 
          request made to the Jama REST API.

    Subclass of Exception built-in exception.
    """

    def __init__(self, status_code, request_url):
        """ Constructor of UnhandledResponseError.

        @param status_code: <int> The status code of respone for the request that was made
        @param request_url: <string> The URL of the request that was made
        """
        self.message = "Unhandled Response Status Code encountered with the Jama REST API. " + \
                       "Status Code: " + str(status_code) + " encountered when making the " + \
                       "URL request [" + request_url + "]."
        super(UnhandledResponseError, self).__init__(self.message)



