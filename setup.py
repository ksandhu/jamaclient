#!/usr/bin/env python3

from setuptools import setup
from setuptools import find_packages

setup(
    name = "jamaclient",
    version = "0.1",
    description = "Jama Rest Client for Python",
    long_description=open('README.md').read(),
    author=['Kate Sandhu'],
    author_email=['katelyn.sandhu@byton.com'],
    packages = find_packages(),
    scripts = [],
    install_requires = open('requirements.txt').read(),
)
