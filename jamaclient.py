"""@package docstring

Created on Feb 14, 2018

@author: katelyn.sandhu

@description: Generic Client for Jama REST API.

"""

# Generic Python Imports
import json

from argparse import ArgumentParser

# Extended Python Imports
import requests

# Byton Imports
import jamaexceptions


class JamaResponseCodes(object):
    """ Jama Response Codes are the responses returned by making a
          request to the Jama REST API.
    """
    RESPONSE_OK = 200  # GET or PUT requested processed successfully
    RESPONSE_CREATED = 201  # POST request processed successfully
    RESPONSE_NO_CONTENT = 204  # DELETE request processed successfully
    RESPONSE_BAD_REQUEST = 400  # The request could not be parsed or parameters were invalid.
    RESPONSE_UNAUTHORIZED = 401  # Username/password is invlaid or user does not have access.
    RESPONSE_NOT_FOUND = 404  # Syntax is correct, but does not exist at specified location.
    RESPONSE_METHOD_NOT_ALLOWED = 405  # There is an issue with the way the request was made.
    RESPONSE_TOO_MANY_REQUESTS = 429  # API throttling or system maintenance.


class JamaItemTypes(object):
    """ Jama Item Types contains all Jama item types and their associated API IDs.
    """
    ATTACHMENT = 22
    COMPONENT = 30
    SET = 31
    FOLDER = 32
    TEXT = 33
    CORE = 34
    TEST_PLAN = 35
    TEST_CYCLE = 36
    TEST_RUN = 37
    VEHICLE_REQUIREMENT = 126
    SYSTEM_REQUIREMENT = 127
    SUBSYSTEM_REQUIREMENT = 132
    COMPONENT_REQUIREMENT = 133
    HARDWARE_REQUIREMENT = 133
    SOFTWARE_REQUIREMENT = 133
    SPECIFICATION = 134
    STANDARD = 134
    REGULATION = 134
    PLAN_OF_RECORD = 135
    VALIDATION_VERIFICATION = 136
    MARKETING_REQUIREMENT = 137
    PRIMARY_ATTRIBUTE_LEADERSHIP = 137
    USER_STORY = 138


class JamaClient(object):
    """ Jama Client abstracts the Jama REST API.  Use this client to make calls
          to the Jama REST API without manually constructing URL requests.
    """

    BASE_URL_DEFAULT = 'https://fmc.jamacloud.com/rest/latest/'

    def __init__(self, config):
        """ Constructor for JamaClient.

        @param config: <dict> Dictionary containing the information required to use Jama's REST API
                         Required Fields: 'username', 'password'
                         Optional Fields: 'base url', 'project id', 'item type id'
        """
        self._check_config_info(config)
        self.username = config['username']
        self.password = config['password']  # TODO: Improve security
        self.base_url = config.get('base url', self.BASE_URL_DEFAULT)

    ''' Initializers '''

    def _check_config_info(self, config):
        """ Check Config Info verifies the configuration information.

        @param config: <dict> Dictionary of configuration information

        @throws: jamaexceptions.InputError if <config> is missing required fields
        """
        if not config.get("username"):
            raise jamaexceptions.InputError("Input Dictionary does not contain 'username' entry.\n")
        if not config.get("password"):
            raise jamaexceptions.InputError("Input Dictionary does not contain the 'password' entry.\n")
        if not config.get("base url"):
            pass

    ''' Main Functions '''

    #!!!!!!!!!!!!!!!!!!!!#
    #!!!!! Projects !!!!!#
    #!!!!!!!!!!!!!!!!!!!!#

    def get_all_projects(self):
        """ Get All Projects requests all of the projects in the Jama Instance.

        @returns projects: <list of dicts>, empty list if no results
        """
        resource = "projects"

        results = self._make_request(resource)
        projects = self._parse_results(results)

        return projects

    def get_project_by_id(self, project_id):
        """ Get Project By ID requests the project specified by a project's specific API ID.

        @param project_id: <int> The API ID of the project to return.
        @returns project: <dict>, None if specified project is not found
        """
        project = None
        resource = "projects/" + str(project_id)
        project_data = self._make_request(resource)
        if len(project_data):
        	project = project_data.pop()

        return project

    #!!!!!!!!!!!!!!!!!!!!!!#
    #!!!!! Item Types !!!!!#
    #!!!!!!!!!!!!!!!!!!!!!!#

    def get_all_item_types_in_instance(self):
        """ Get All Item Types in Instance requests all of the item types from the Jama Instance.

        @returns <list of dicts>, empty list if no results
        """
        resource = "itemtypes"

        results = self._make_request(resource)
        itemtypes = self._parse_results(results)

        return itemtypes
        
    def get_all_item_types_in_project(self, project_id):
        """ Get All Item Types in Project requests all of the item types available in the
              specified project.

        @param project_id: <int> The API ID of the project to get the item types for.
        @returns <list of dicts>, empty list if no results
        """
        resource = "projects/" + str(project_id) + "/itemtypes"

        results = self._make_request(resource)
        itemtypes = self._parse_results(results)
        
        return itemtypes

    #!!!!!!!!!!!!!!!!!#
    #!!!!! Items !!!!!#
    #!!!!!!!!!!!!!!!!!#

    def get_all_items_in_instance(self):
        """ Get All Items in Instance requests all items in the Jama instance.

        @returns <list of dicts>. empty list if no results
        """
        resource = "abstractitems"

        results = self._make_request(resource)
        items = self._parse_results(results)

        return items

    def get_all_items_by_project(self, project_id):
        """ Get All Items By Project requests all items within the specified project.

        @param project_id: <int> The ID of the project to get the items for.
        @returns <list of dicts>, empty list if no results 
        """
        resource = "abstractitems"
        params = "project=" + str(project_id)

        results = self._make_request(resource, params)
        items = self._parse_results(results)

        return items

    def get_all_items_of_type_from_instance(self, item_type_id):
        """ Get All Items OF Type From Instance requests all items that are of the type specified by the item type API ID.

        @param item_type_id: <string> The API ID of the item type to return all items of.
        @returns <list of dicts>, empty list if no results
        """
        resource = "abstractitems"
        params = "itemType=" + str(item_type_id)

        results = self._make_request(resource, params)
        items = self._parse_results(results)

        return items


    def get_all_items_of_type_from_project(self, project_id, item_type_id):
        """ Get All Items Of Type from Project returns all items with the specified Item Type API ID
              that exist within the project with the specified Project API ID.

        @param project_id: <int> The project API ID to get items from.
        @param item_type_id: <int> The item type API ID of the items to return.
        @returns <list of dicts>, empty list if no results
        """
        resource = "abstractitems"
        params = "project=" + str(project_id) + "&itemType=" + str(item_type_id)

        results = self._make_request(resource, params)
        items = self._parse_results(results)

        return items

    def get_item_by_id(self, item_id):
        """ Get Item By ID requests a specific item with the specified API ID.

        @param item_id: <string> The API ID of the item to return.
        @returns <dict>, None if specified item is not found
        """
        item = None
        resource = "abstractitems/" + str(item_id)
        item_data = self._make_request(resource)
        if len(item_data):
            item = item_data.pop()

        return item

    #!!!!!!!!!!!!!!!!!!!!!!#
    #!!!!! Test Plans !!!!!#
    #!!!!!!!!!!!!!!!!!!!!!!#

    def get_all_testplans_in_project(self, project_id):
        """ Get All Testplans in Project returns all test plans within the specified project.

        @param project_id: <int> The API ID of the project to return the test plans for.
        @returns <list of dicts>, empty list if no results
        """
        resource = "testplans"
        params = "project=" + str(project_id)

        results = self._make_request(resource, params)
        testplans = self._parse_results(results)

        return testplans

    def get_testplan_by_id(self, testplan_id):
        """ Get Testplan By ID returns the test plan with the specified API ID.

        @param testplan_id: <int> The API ID of the testplan to return.
        @returns <dict>, None if specified testplan is not found
        """
        testplan = None
        resource = "testplans/" + str(testplan_id)

        testplan_data = self._make_request(resource)
        if len(testplan_data):
            testplan = testplan_data.pop(0)

        return testplan

    def get_test_cycles_for_testplan(self, testplan_id):
        """ Get Test Cycles For Testplan returns all test cycles for the testplan with the specified API ID.

        @param testplan_id: <int> The API ID of the testplan to return.
        @returns <list of dicts>, empty list if no results
        """
        resource = "testplans/" + str(testplan_id) + "/testcycles"

        results = self._make_request(resource)
        test_cycles = self._parse_results(results)

        return test_cycles

    def get_test_groups_for_testplan(self, testplan_id):
        """ Get Test Groups For Testplan returns all test group for the testplan with the specified API ID.

        @param testplan_id: <int> The API ID of the testplan to return.
        @returns <list of dicts>, empty list if no results
        """
        resource = "testplans/" + str(testplan_id) + "/testgroups"

        results = self._make_request(resource)
        test_groups = self._parse_results(results)

        return test_groups

    def get_test_cases_for_test_group(self, testplan_id, test_group_id):
        """ Get Test Cases for Test Group returns all test cases for the specified test group
              in the specified test plan.

        @param testplan_id: <int> The API ID of the testplan to get the test groups for.
        @param test_group_id: <int> The API ID of the test group to get the test cases for.
        @returns <list of dicts>, empty list if no results
        """
        resource = "testplans/" + str(testplan_id) + "/testgroups/" + str(test_group_id) + "/testcases"

        results = self._make_request(resource)
        test_cases = self._parse_results(results)

        return test_cases

    #!!!!!!!!!!!!!!!!!!!!!!!#
    #!!!!! Test Cycles !!!!!#
    #!!!!!!!!!!!!!!!!!!!!!!!#

    def get_test_cycle_by_id(self, test_cycle_id):
        """ Get Test Cycle by ID returns the test cycle associated with the specified test cycle API ID.

        @param test_cycle_id: <int> The API ID of the test cycle to return.
        @returns <dict>, None if specified test cycle is not found
        """
        test_cycle = None
        resource = "testcycles/" + str(test_cycle_id)

        test_cycle_data = self._make_request(resource)
        if len(test_cycle_data):
            test_cycle = test_cycle_data.pop()

        return test_cycle


    def get_test_runs_for_test_cycle(self, test_cycle_id):
        """ Get Test Runs for Test Cycle returns the test runs for the test cycle with the specified API ID.

        @param test_cycle_id: <int> the test cycle API ID to get the test runs for
        @returns <list of dicts>, empty list if no test runs
        """
        resource = "testcycles/" + str(test_cycle_id) + "/testruns"

        results = self._make_request(resource)
        test_runs = self._parse_results(results)

        return test_runs

    #!!!!!!!!!!!!!!!!!!!!!#
    #!!!!! Test Runs !!!!!#
    #!!!!!!!!!!!!!!!!!!!!!#

    def get_test_runs(self):
        """ Get Test Runs returns all test runs in an instance.

        @returns <list of dicts>, empty list if no test runs
        """
        resource = "testruns"

        results = self._make_request(resource)
        test_runs = self._parse_results(results)

        return test_runs

    def get_test_runs_for_testplan(self, testplan_id):
        """ Get Test Runs For Testplan returns all test runs for the specified Testplan API ID.

        @param testplan_id: <int> The Testplan API ID to get the test runs for
        @returns <list of dicts>, empty list if no test runs
        """
        resource = "testruns"
        params = "testPlan=" + str(testplan_id)

        results = self._make_request(resource, params)
        test_runs = self._parse_results(results)

        return test_runs


    def get_test_runs_for_test_case(self, test_case_id):
        """ Get Test Runs For Test Case returns all test runs for the specified Test Case API ID.

        @param test_case_id: <int> The Test Case API ID to get the test runs for
        @returns <list of dicts>, empty list if no test runs
        """
        resource = "testruns"
        params = "testCase=" + str(test_case_id)

        results = self._make_request(resource)
        test_runs = self._parse_results(results)

        return test_runs

    def get_test_run_by_id(self, test_run_id):
        """ Get Test Run by ID returns the test run associated with the specified Test Run API ID.

        @param test_run_id: <int> The Test Run API ID of the test run to return
        @returns <dict>, None if test run does is not found
        """
        resource = "testruns/" + str(test_run_id)

        test_run_data = self._make_request(resource)
        if len(test_run_data):
            test_run = test_run_data.pop()

        return test_run

    #!!!!!!!!!!!!!!!!#
    #!!!!! Tags !!!!!#
    #!!!!!!!!!!!!!!!!#

    def get_all_items_with_tag(self, tag_id):
        """ Get All Items With Tag returns all items with the tag associated with the specified Tag API ID.

        @param tag_id: <int> The Tag API ID to return all items associated with it.
        @returns <list of dicts>, empty if no items associated with specified tag
        """
        resource = "tags/" + str(tag_id) + "/items"

        results = self._make_request(resource)
        items = self._parse_results(results)

        return items

    def get_all_tags_for_project(self, project_id):
        """ Get All Tags For Project returns all tags within the project specified by the Project API ID.

        @param project_id: <int> The Project API ID of the project to get all tags for.
        @returns <list of dicts>, empty list if no tags found for specified project
        """
        resource = "tags"
        params = "project=" + str(project_id)

        results = self._make_request(resource, params)
        tags = self._parse_results(results)

        return tags

    ''' Helper Functions '''
    def _make_request(self, resource, parameters=None):
        """ Make Request makes the specified request, with pagination.  The results are returned.

        @param resource: <string> the resource to request
        @param parameters: <string> the parameters to include, defaults to None (i.e. projects)
                                    if there are not any parameters, None
                                    parameters should be formatted as follows: "param1=value1&param2=value2"
        @return results: <list of dicts> the response to the request, where each entry in the list is a "page" of
                                           of up to 50 responses
        """
        # Allow maximum number if results per response
        allowed_results = 50
        max_results = "maxResults=" + str(allowed_results)
        result_count = -1
        start_index = 0

        # Set Parameters string
        if parameters:
            params = parameters
        else:
            params = ""

        results = []

        while result_count != 0:
            start_at = "startAt=" + str(start_index)
            # Set up the request URL with resources, parameters, and pagination
            url = self.base_url + resource + "?" + params + "&" + start_at + "&" + max_results
            response = requests.get(url, auth=(self.username, self.password))

            self._check_status_code(response.status_code, url)

            # JSONify the response and parse result information
            json_response = response.json()
            if "pageInfo" in json_response["meta"]:
                page_info = json_response["meta"]["pageInfo"]
                start_index = page_info["startIndex"] + allowed_results
                result_count = page_info["resultCount"]
            else:
                result_count = 0

            # Add page of JSON results to overall results list
            results.append(json_response)

        return results

    def _check_status_code(self, status_code, request_url):
        """ Check Status Code verifies that the response was returned successfully.
              If it was not, an exception is thrown.

        @param status_code: <int> The status code of respone for the request that was made
        @param request_url: <string> The URL of the request that was made

        @throws: jamaexceptions.BadRequestResponseError if request could not be parsed or
                   parameters were not valid
        @throws: jamaexceptions.UnauthorizedResponseError if username/password is invalid
                   or user does not have access to requested object
        @throws: jamaexceptions.NotFoundResponseError if the object requested does not exist
                   at the specified location
        @throws: jamaexceptions.MethodNotAllowedResponseError if there is an issue with the
                   way the request was made
        @throws: jamaexceptions.TooManyRequestsResponseError if there is API throttling or
                   system maintenance
        @throws: jamaexceptions.UnhandledResponseError for all other error responses to requests
        """
        if status_code == JamaResponseCodes.RESPONSE_OK:  # Successful GET or PUT
            pass
        elif status_code == JamaResponseCodes.RESPONSE_CREATED:  # Successful POST
            pass
        elif status_code == JamaResponseCodes.RESPONSE_NO_CONTENT:  # Successful DELETE
            pass
        elif status_code == JamaResponseCodes.RESPONSE_BAD_REQUEST:
            # The request could not be parsed or the parameters were not valid.
            raise jamaexceptions.BadRequestResponseError(request_url)
        elif status_code == JamaResponseCodes.RESPONSE_UNAUTHORIZED:
            # Username/password is invalid, or the user does not have access to the requested object
            raise jamaexceptions.UnauthorizedResponseError(request_url)
        elif status_code == JamaResponseCodes.RESPONSE_NOT_ALLOWED:
            # Syntax is correct on the request, but does not exist at the location specified
            raise jamaexceptions.NotFoundResponseError(request_url)
        elif status_code == JamaResponseCodes.RESPONSE_METHOD_NOT_ALLOWED:
            # There is an issue with the way the request was made
            raise jamaexceptions.MethodNotAllowedResponseError(request_url)
        elif status_code == JamaResponseCodes.RESPONSE_TOO_MANY_REQUESTS:
            # Returned if the API is being throttled or the system is down for maintenance.
            raise jamaexceptions.TooManyRequestsResponseError(request_url)
        else:
            raise jamaexceptions.UnhandledResponseError(status_code, request_url)


    def _parse_results(self, results):
        """ Parse Results parses a list of JSON results returned from _make_request.

        @param results: <list of dicts> the response from the request returned from _make_request
        @returns <list of dicts> list of the actual request types in dict format
        """
        items = []
        for result in results:
            data = result['data']
            for item in data:
                items.append(item)

        return items


def main(config_file, password):
    """ Main is an example of usage for the JamaClient.  It can be run from the command line, but as is, doesn't
          do anything very useful.
    """
    item_type_id = None
    project_id = None

    with open(config_file) as config:
        config = config.read()
        config = json.loads(config)

    config['password'] = password

    jama_client = JamaClient(config)

    projects = jama_client.get_all_projects()
    item_types = jama_client.get_all_item_types_in_instance()
    # items = jama_client.get_all_items_in_instance()  ## Note: This takes a long time
    test_runs = jama_client.get_test_runs()

    if "project id" in config:
        project_id = config["project id"]
        project = jama_client.get_project_by_id(project_id)
        project_item_types = jama_client.get_all_item_types_in_project(project_id)
        # project_items = jama_client.get_all_items_by_project(project_id)  ## Note: This takes a long time
        tags = jama_client.get_all_tags_for_project(project_id)
        if tags:
            # Get the first tag in the list
            tag= tags.pop(0)
            tag_id = tag["id"]
            # Retrieve all items with specified tag ID
            items = jama_client.get_all_items_with_tag(tag_id)


        testplans = jama_client.get_all_testplans_in_project(project_id)
        if testplans:
            # Get the first testplan in the list
            testplan = testplans.pop(0)
            testplan_id = testplan["id"]
            # Retrieve testplan from testplan's API ID
            testplan = jama_client.get_testplan_by_id(testplan_id)
            test_runs = jama_client.get_test_runs_for_testplan(testplan_id)

            # Get Test Cycles for Test Plan
            test_cycles = jama_client.get_test_cycles_for_testplan(testplan_id)
            # Get Test Groups for Test Plan
            test_groups = jama_client.get_test_groups_for_testplan(testplan_id)

            if test_cycles:
                # Get first test cycle in list
                test_cycle = test_cycles.pop(0)
                test_cycle_id = test_cycle["id"]
                # Retrieve test cycle from the test cycle's API ID
                test_cycle = jama_client.get_test_cycle_by_id(test_cycle_id)
                # Retrieve test runs for test cycle
                test_runs = jama_client.get_test_runs_for_test_cycle(test_cycle_id)

            if test_groups:
                # Get the first test group in the list
                test_group = test_groups.pop(0)
                test_group_id = test_group["id"]
                # Retrieve test cases for test group
                test_cases = jama_client.get_test_cases_for_test_group(testplan_id, test_group_id)

                if test_cases:
                    # Get the first test case in the list
                    test_case = test_cases.pop(0)
                    test_case_id = tst_case["id"]
                    # Get test runs for the test case
                    test_runs = jama_client.get_test_runs_for_test_case(test_case_id)

                if test_runs:
                    # Get the first test run in the list
                    test_run = test_runs.pop(0)
                    test_run_id = test_run["id"]
                    # Get test run by ID
                    test_run = jama_client.get_test_run_by_id(test_run_id)


    if "item type id" in config:
        item_type_id = config["item type id"]
        # items = jama_client.get_all_items_of_type_from_instance(item_type_id)  ## Note: This takes a long time

    if item_type_id and project_id:
        items = jama_client.get_all_items_of_type_from_project(project_id, item_type_id)

        if items:
            # Get the first item in the list
            first_item = items.pop(0)
            item_api_id = first_item["id"]
            # Retrieve item from item's API ID
            item = jama_client.get_item_by_id(item_api_id)


def argparser():
    argparser = ArgumentParser(description="Generic Client for Jama REST API")
    argparser.add_argument('-c', '--config', help='Full path to JSON Config File.', required=True)
    argparser.add_argument('-p', '--password', help='Your Jama password.', required=True)
    return argparser

if __name__ == "__main__":
    args = argparser().parse_args()
    main(args.config, args.password)
